var express = require("express");
var router = express.Router();
require("./addEmployee");


router.post('/', function (request , response) {
    // response.end("in hr view");
    response.send(employeeArray);
    // employeeArray.forEach(function (employee) {
    //     // response.write(JSON.stringify(employee));
    //         response.json(employee);
    // });
    response.end();
});

router.get('/', function (request, response) {
    //response.send(request.query);
    var found = false;
    var resultArray = new Array();
    employeeArray.forEach(function (employee) {
        if (employee.firstName.indexOf(request.query.search) >= 0) {
            found = true;
            resultArray.push(employee);
        }
    });
    if(!found)
    response.end("No Employee with such name");
    else
        response.send(resultArray);
});

module.exports = router;