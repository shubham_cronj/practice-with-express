var express = require("express");
var router = express.Router();
var view = require('./hrRoutes/view');
var createEmployee = require('./hrRoutes/addEmployee');
var giveRemarks = require('./hrRoutes/giveRemarks');

router.use('/view', view);
router.use('/addEmployee', createEmployee);
router.use('/giveRemarks', giveRemarks);


router.post('/', function (request , response) {
   response.end("in hr");
});

module.exports = router;