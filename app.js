var express = require('express');
var app = express();
var pg = require("pg");
var login = require("./routes/login");
var hrController = require("./routes/hrController");
var employeeController = require("./routes/employeeController");

var bodyParser = require("body-parser");
require("./constants");


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended : false }));

app.use('/', login);
app.use('/hr', hrController);
app.use('/employee', employeeController);

app.listen(3000,function(){
    console.log("listening on port 3000");
});

var conString = "pg://postgres:postgres@localhost:5432/employees";
var client = new pg.Client(conString);
client.connect();